pragma solidity ^0.4.6;

import "./Stoppable.sol";

contract Vehicles is Stoppable {
    
    struct Vehicle {
        address owner;
        address driver;
        bytes32 vehiculeType; // e.g. Car, Truck or Motocycle
    }
    

    bytes32[] public vehiculesList;
    mapping(bytes32 => Vehicle) vehiculesRegistry;
    mapping(bytes32 => uint) vehiculeTypeTax;
    
    function Vehicles(uint taxCar, uint taxTruck, uint taxMotocycle)
        public
    {
        vehiculeTypeTax["Car"] = taxCar;
        vehiculeTypeTax["Truck"] = taxTruck;
        vehiculeTypeTax["Motocycle"] = taxMotocycle;
    }

    function getVehicleCount ()
        public
        constant
        returns(uint vehicleCount)
    {
        return vehiculesList.length;
    }
    
    function changeVehicleTax(bytes32 vehiculeType, uint vehicleTax)
        onlyOwner
        onlyIfRunning
        returns(bool)
    {
        require(vehicleTax > 0);
        
        vehiculeTypeTax[vehiculeType] = vehicleTax;
        
        return true;
    }

    
    function getVehicleTax(bytes32 licencePlate)
        public
        constant
        returns(uint)
    {
        require(vehiculesRegistry[licencePlate].owner != 0);
        return vehiculeTypeTax[vehiculesRegistry[licencePlate].vehiculeType];
    }
    
    function addVehicle(bytes32 licencePlate, address owner, bytes32 Type)
        onlyOwner
        onlyIfRunning
        returns(bool)
    {
        //check licence plate has not been assigned
        require(vehiculesRegistry[licencePlate].owner == 0);
        
        //check Owner address
        require(owner != 0);
        
        //check vehiculeType
        require(vehiculeTypeTax[Type] != 0);
        
        vehiculesRegistry[licencePlate].owner = owner;
        vehiculesRegistry[licencePlate].driver = owner;
        vehiculesRegistry[licencePlate].vehiculeType = Type;
        vehiculesList.push(licencePlate);
        
        return true;
    }
    
    function changeDriver(bytes32 licencePlate, address driver)
        onlyIfRunning
        returns(bool)
    {
        //check owner
        require(vehiculesRegistry[licencePlate].owner == msg.sender);
       
        //check driver address
        require(driver != 0);
        
        vehiculesRegistry[licencePlate].driver = driver;
        
        return true;
    }
 
    function changeVehicleOwner(bytes32 licencePlate, address newOwner)
        onlyIfRunning
        returns(bool)
    {
        //check owner
        require(vehiculesRegistry[licencePlate].owner == msg.sender);

        vehiculesRegistry[licencePlate].owner = newOwner;
        
        return true;
    }

    function getVehicleInfo(bytes32 licencePlate)
        public
        constant
        returns(address Owner, address Driver, bytes32 Type)
    {
        require(vehiculesRegistry[licencePlate].owner != 0);

        return (vehiculesRegistry[licencePlate].owner, vehiculesRegistry[licencePlate].driver, vehiculesRegistry[licencePlate].vehiculeType);
    }

     function getDriver(bytes32 licencePlate)
        public
        constant
        returns(address Driver)
    {
        require(vehiculesRegistry[licencePlate].driver != 0);
        return (vehiculesRegistry[licencePlate].driver);
    }   
    
}