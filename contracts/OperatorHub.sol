pragma solidity ^0.4.6;

import "./Toll.sol"; // Imported to create Toll contracts (which include RegulatorHub.sol)

// Owner is the Regulator Hub
contract OperatorHub is Stoppable {
    
    // 1. Variables & Events & Modifiers
    
    address public  operatorAddress;
    address public  vehicleRegistryAddress;
    uint    public  minTollBaseFees;
    uint    public  maxTollBaseFees;
    uint    public  shareToRegulator;
    uint    public  shareToRegulator_OperatorProposition;
    uint    public  shareToRegulator_RegulatorProposition;
    
    address[] public tolls;
    mapping(address => bool) tollExists;
    
    modifier onlyIfToll(address toll) { require(tollExists[toll]); _; }
    modifier onlyIfOperator { require(operatorAddress == msg.sender); _; }
    
    // 2. Constructor

    // Inputs: operatorAddress defines the operator that will control the OperatorHub contract.
    // + Sets the share in % allocated to the regulator each time a driver pays a toll (between 0 and 100), and sets the range in which an operator can set toll fees (in wei).
    // Example: operatorAddress="0x8888", operatorAddress="0x7777", shareToRegulator=20, minTollBaseFees=0, maxTollBaseFees=1000000
    function OperatorHub(address operator, address vehicleRegistry, uint _shareToRegulator, uint _minTollBaseFees, uint _maxTollBaseFees)
        public
    {
        require(_shareToRegulator >= 0);
        require(_shareToRegulator <= 100);
        require(_minTollBaseFees >= 0);
        require(_maxTollBaseFees >= 0);
        require(_minTollBaseFees < _maxTollBaseFees);
        require(operator != 0);
        
        operatorAddress = operator;
        vehicleRegistryAddress = vehicleRegistry;
        shareToRegulator = _shareToRegulator;
        minTollBaseFees = _minTollBaseFees;
        maxTollBaseFees = _maxTollBaseFees;
    }
    
    // 3. Getters

    function getTollCount ()
        public
        constant
        returns(uint tollCount)
    {
        return tolls.length;
    }
    
    function getOperatorAddress()
        public
        constant
        returns(address)
    {
        return operatorAddress;
    }
    
    function getshareToRegulator()
        public
        constant
        returns (uint)
    {
        return shareToRegulator;
    }
    
    function getIntervalFees()
        public
        constant
        returns (uint min, uint max)
    {
        return (minTollBaseFees, maxTollBaseFees);
    }    

    // 4. Setters
    
    // The regulatorHub (owner) or the operator (operatorAddress) are able to propose a "shareToRegulator". If their proposition match then it applies to the "shareToRegulator"
    // Example: shareProposition=50
    function setShareToRegulator(uint shareProposition)
        public
        returns(bool success)
    {
        require(msg.sender == operatorAddress || msg.sender == owner);
        require(shareProposition >= 0);
        require(shareProposition <= 100);
        
        if (msg.sender == operatorAddress)
        {
            shareToRegulator_OperatorProposition = shareProposition;
        }
        else if (msg.sender == owner)
        {
            shareToRegulator_RegulatorProposition = shareProposition;
        }
        
        if (shareToRegulator_OperatorProposition == shareToRegulator_RegulatorProposition)
        {
            shareToRegulator = shareToRegulator_OperatorProposition;
            return true;
        }
        else
        {
            return false;
        }
    }

    // 4.1. Change contract variables from regulatorHub

    // Change the range in which an operator can set toll fees (in wei)
    // Example: operatorHub="0x8888", minTollBaseFees=1000, maxTollBaseFees=5000000
    function setIntervalTollFees(uint _minTollBaseFees, uint _maxTollBaseFees)
        onlyOwner
        onlyIfRunning
        returns(bool success)
    {
        require(_minTollBaseFees > _maxTollBaseFees);
        
        minTollBaseFees = _minTollBaseFees;
        maxTollBaseFees = _maxTollBaseFees;
        return true;
    }

    // Call from an operatorHub (msg.sender) through regulatorHub to transfer a toll ownership
    // Example: tollAddress="0x8888"
    function addTollOwnership(address tollAddress)
        onlyOwner
        onlyIfRunning
        returns(bool success)
    {
        require(!tollExists[tollAddress]);
         
        tolls.push(tollAddress);
        tollExists[tollAddress] = true;
        return true;
    }
    
    // 4.2. Toll Controller from operator
    
    // Deploy a toll booth and set a toll fees in wei
    // Example: tollBaseFees=5000
    function addToll(uint tollBaseFees)
        onlyIfOperator
        onlyIfRunning
        returns(address toll)
    {
        require(minTollBaseFees < tollBaseFees);
        require(tollBaseFees < maxTollBaseFees);
        
        Toll trustedToll = new Toll(owner, vehicleRegistryAddress, tollBaseFees);
        tolls.push(trustedToll);
        tollExists[trustedToll] = true;
        return trustedToll;   
    }
    
    // Change toll fees in wei
    // Example: tollAddress="0x8888", newTollBaseFees=10000
    function changeTollFees(address tollAddress, uint newTollBaseFees)
        onlyIfOperator
        onlyIfToll(tollAddress)
        onlyIfRunning
        returns(bool success)
    {
        require(minTollBaseFees < newTollBaseFees);
        require(newTollBaseFees < maxTollBaseFees);
        
        Toll trustedToll = Toll(tollAddress);
        return(trustedToll.changeTollFees(newTollBaseFees));
    }

    // Switch on/off a toll booth
    // Example: tollAddress="0x8888", onOff=0
    function runSwitchToll(address tollAddress, bool onOff)
        onlyIfOperator
        onlyIfToll(tollAddress)
        onlyIfRunning
        returns(bool success)
    {
        Toll trustedToll = Toll(tollAddress);
        return(trustedToll.runSwitch(onOff));
    }
    
    // Change the ownership of a toll booth
    // Example: tollAddress="0x8888", newOperatorHubOwner="0x7777"
    function changeTollOwner(address tollAddress, address newOperatorHubOwner)
        onlyIfOperator
        onlyIfToll(tollAddress)
        onlyIfRunning
        returns(bool success)
    {
        require(owner != 0);
        require(owner != newOperatorHubOwner);

        RegulatorHub trustedRegulatorHub = RegulatorHub(owner);
        
        require(trustedRegulatorHub.isOperatorHub(newOperatorHubOwner));
        
        Toll trustedToll = Toll(tollAddress);
        require(trustedToll.changeOwner(newOperatorHubOwner));

        tollExists[tollAddress] = false;

        trustedRegulatorHub.changeTollOwnership(tollAddress, newOperatorHubOwner);

        return true;
    }

        
}