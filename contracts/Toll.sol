pragma solidity ^0.4.6;

import "./RegulatorHub.sol"; // Imported to retrive the regulator address (which include Vehicles, OperatorHub and Stoppable)

// Owner is the Operator Hub
contract Toll is Stoppable {
    
    // 1. Variables & Events
        
    address public  regulatorHubAddress; // Address of the regulator hub - used to retrive the regulator address
    address public  vehicleRegistryAddr; // Address of the vehicle registry - used to retrive the vehicle type
    uint    public  tollBaseFees; // Toll base fees (regardless the type of vehicle) in Wei
    
    event LogPayToll(uint totalPaid, uint amountToRegulator, uint amountToOperator);
    
    // 2. Constructor
    
    function Toll(address tollRegulator, address tollVehicleRegistry, uint _tollBaseFees)
    {
        regulatorHubAddress = tollRegulator;
        vehicleRegistryAddr = tollVehicleRegistry;
        tollBaseFees = _tollBaseFees;
    }
    
    // 3. Getters
    
    // Called from the Pay function to retrive minTollBaseFees and maxTollBaseFees, retrive the vehicle type and the drivers address.
    function getExpectedFees(bytes32 licencePlate)
        public
        constant
        returns(uint, uint, address)
    {
        uint    Fees;
        Vehicles trustedVehicles = Vehicles(vehicleRegistryAddr);
        address Driver = trustedVehicles.getDriver(licencePlate);
        require(Driver == msg.sender);

        uint vehicleTypeTax = trustedVehicles.getVehicleTax(licencePlate);
        
        OperatorHub trustedOperator = OperatorHub(owner);
        var (minTollBaseFees, maxTollBaseFees) = trustedOperator.getIntervalFees();
        uint shareToRegulator = trustedOperator.getshareToRegulator();
        address operatorAddress = trustedOperator.getOperatorAddress();
        
        if(minTollBaseFees < tollBaseFees && tollBaseFees < maxTollBaseFees)
        {
            Fees = tollBaseFees;
        }
        else if (tollBaseFees < minTollBaseFees)
        {
            Fees = minTollBaseFees;
        }
        else if (maxTollBaseFees < tollBaseFees)
        {
            Fees = maxTollBaseFees;
        }
        
        Fees = Fees * (1 + vehicleTypeTax / 100);

        return (Fees, shareToRegulator, operatorAddress);
    }
    
    // 4. Setters
    
    // Set new toll fees in wei from operatorHub (owner)
    // Example: newTollFees=10000
    function changeTollFees(uint newTollFees)
        onlyOwner
        onlyIfRunning
        returns(bool success)
    {
        tollBaseFees = newTollFees;
        return true;
    }

    // A driver pays a toll booth through this function
    // Example: licencePlate="test8888"
    function Pay(bytes32 licencePlate)
        public
        onlyIfRunning
        payable
        returns(bool success)
    {
        var (ExpectedFees, shareToRegulator, operatorAddress) =  getExpectedFees(licencePlate);
        
        require(msg.value == ExpectedFees);
        
        RegulatorHub trustedRegulator = RegulatorHub(regulatorHubAddress);
        address regulatorAddress = trustedRegulator.whoIsOwner();
        uint amountToRegulator = ExpectedFees * shareToRegulator / 100;
        uint amountToOperator = ExpectedFees - amountToRegulator; 

        regulatorAddress.transfer(amountToRegulator);
        operatorAddress.transfer(amountToOperator);
        
        LogPayToll(ExpectedFees, amountToRegulator, amountToOperator);
        return true;   
    }

}