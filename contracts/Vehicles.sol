pragma solidity ^0.4.6;

import "./Stoppable.sol"; // Imported to add the runSwitch function and refactor the Owned code

// Owner is the Regulator Hub
contract Vehicles is Stoppable {
    
    // 1. Variables & Events & Modifiers
    
    struct Vehicle {
        address owner; // e.g. "test8888"
        address driver; // e.g. "test7777"
        bytes32 vehiculeType; // e.g. Car, Truck or Motocycle
        // bool    running; 
    }
    
    bytes32[] public vehiculesList;
    mapping(bytes32 => Vehicle) vehiculesRegistry;
    mapping(bytes32 => uint) vehiculeTypeTax;
    
    event LogVehicleAdded(bytes32 licencePlate);
    
    modifier onlyIfVehicleOwner(bytes32 licencePlate) { require(vehiculesRegistry[licencePlate].owner == msg.sender); _; }
    
    // 2. Constructor
    
    // For simplicity, the contract define the extra fees for Cars, Trucks and Motocycles by default.
    // Example: taxCar=100, taxTruck=150, taxMotocycle=50
    // A Truck driver will pay BaseFees * (1 + taxTruck / 100) wei.     
    function Vehicles(uint taxCar, uint taxTruck, uint taxMotocycle)
        public
    {
        vehiculeTypeTax["Car"] = taxCar;
        vehiculeTypeTax["Truck"] = taxTruck;
        vehiculeTypeTax["Motocycle"] = taxMotocycle;
    }

    // 3. Getters
    
    function getVehicleCount()
        public
        constant
        returns(uint vehicleCount)
    {
        return vehiculesList.length;
    }

    function getVehicleTax(bytes32 licencePlate)
        public
        constant
        returns(uint)
    {
        require(vehiculesRegistry[licencePlate].owner != 0);
        return vehiculeTypeTax[vehiculesRegistry[licencePlate].vehiculeType];
    }

    function getVehicleInfo(bytes32 licencePlate)
        public
        constant
        returns(address vehicleOwner, address vehicleDriver, bytes32 vehicleType)
    {
        require(vehiculesRegistry[licencePlate].owner != 0);

        return (vehiculesRegistry[licencePlate].owner, vehiculesRegistry[licencePlate].driver, vehiculesRegistry[licencePlate].vehiculeType);
    }
    
    function getDriver(bytes32 licencePlate)
        public
        constant
        returns(address Driver)
    {
        require(vehiculesRegistry[licencePlate].driver != 0);
        return (vehiculesRegistry[licencePlate].driver);
    } 

    
    // 4. Setters
    
    // 4.1. Functions controlled by the Regulator

    // Add a vehicle in the registry.
    // The vehicle owner will able to define a driver which would pay toll
    // Example: licencePlate="test000", vehicleOwner="0x8888", vehicleType="Truck"
    function addVehicle(bytes32 licencePlate, address owner, bytes32 Type)
        onlyOwner
        onlyIfRunning
        returns(bool success)
    {
        //check if licence plate has not been assigned
        require(vehiculesRegistry[licencePlate].owner == 0);
        
        //check Owner address
        require(owner != 0);
        
        //check vehiculeType
        require(vehiculeTypeTax[Type] != 0);
        
        vehiculesRegistry[licencePlate].owner = owner;
        vehiculesRegistry[licencePlate].driver = owner;
        vehiculesRegistry[licencePlate].vehiculeType = Type;
        vehiculesList.push(licencePlate);
        
        LogVehicleAdded(licencePlate);
        
        return true;
    }
    
    // Add or change a vehicle type and its extra fees charged everytime
    // Example: vehicleType="Truck", vehiculeTax=50
    function addChangeVehiculeType(bytes32 vehiculeType, uint vehicleTax)
        onlyOwner
        onlyIfRunning
        public
        returns(bool success)
    {
        vehiculeTypeTax[vehiculeType] = vehicleTax;
        
        return true;
    }
    

    // 4.2. Functions controlled by a vehicle owner

    // Change the driver that will pay toll
    // Example:  licencePlate="test8888", driver="0x8888"
    function changeDriver(bytes32 licencePlate, address driver)
        onlyIfVehicleOwner(licencePlate)
        onlyIfRunning
        returns(bool)
    {
        //check driver address
        require(driver != 0);
        
        vehiculesRegistry[licencePlate].driver = driver;
        
        return true;
    }
 
    // Change the owner of a veheicle
    // Example:  licencePlate="test8888", newOwner="0x8888"
    function changeVehicleOwner(bytes32 licencePlate, address newOwner)
        onlyIfVehicleOwner(licencePlate)
        onlyIfRunning
        returns(bool success)
    {
        vehiculesRegistry[licencePlate].owner = newOwner;
        return true;
    }
    
}