pragma solidity ^0.4.6;

import "./Vehicles.sol"; // Imported to create a vehicle registry
import "./OperatorHub.sol"; // Imported to create an operator Hub
import "./Stoppable.sol"; // Imported to add the runSwitch function and refactor the Owned code

// Owner is the Regulator
contract RegulatorHub is Stoppable {
    
    // 1. Variables & Events & M
    
    address public vehicleRegistry;
    address[] public operatorsHub;
    mapping(address => bool) operatorHubExists;

    event LogOperatorHubAdded(address operatorHubAddress);

    modifier onlyIfOperatorHub(address operatorHubAddress) { require(operatorHubExists[operatorHubAddress]); _; }
    
    // 2. Constructor

    // For simplicity, the contract define the extra fees for Cars, Trucks and Motocycles by default.
    // Example: taxCar=100, taxTruck=150, taxMotocycle=50
    // A Truck driver will pay BaseFees * (1 + taxTruck / 100) wei. 
    function RegulatorHub(uint taxCar, uint taxTruck, uint taxMotocycle)
        public
    {
        require(taxCar > 0);
        require(taxTruck > 0);
        require(taxMotocycle > 0);

        Vehicles trustedVehicleRegistry = new Vehicles(taxCar, taxTruck, taxMotocycle);
        vehicleRegistry = trustedVehicleRegistry;
    }

    // 3. Getters
    
    // Monitor the number of operators created
    function getOperatorCount()
        public
        constant
        returns(uint operatorCount)
    {
        return operatorsHub.length;
    }

    // Check if an operatorHub exists
    function isOperatorHub(address operatorHubAddress)
        public
        constant
        returns(bool)
    {
        require(operatorHubAddress != 0);

        return operatorHubExists[operatorHubAddress];
    }
        
    // 4. Setters
    
    // 4.1. Control of an operatorHub

    // Inputs: operatorAddress defines the operator that will control the OperatorHub contract.
    // + Sets the share in % allocated to the regulator each time a driver pays a toll (between 0 and 100), and sets the range in which an operator can set toll fees (in wei).
    // Example: operatorAddress="0x8888", shareToRegulator=20, minTollBaseFees=0, maxTollBaseFees=1000000
    function addOperator(address operatorAddress, uint shareToRegulator, uint minTollBaseFees, uint maxTollBaseFees)
        onlyOwner
        onlyIfRunning
        returns(bool success)
    {
        require(shareToRegulator >= 0);
        require(shareToRegulator <= 100);
        require(minTollBaseFees >= 0);
        require(maxTollBaseFees >= 0);
        require(minTollBaseFees < maxTollBaseFees);
        require(operatorAddress != 0);

        OperatorHub trustedOperatorHub = new OperatorHub(operatorAddress, vehicleRegistry, shareToRegulator, minTollBaseFees, maxTollBaseFees);
        operatorsHub.push(trustedOperatorHub);
        operatorHubExists[trustedOperatorHub] = true;
        
        LogOperatorHubAdded(trustedOperatorHub);
        return true;
    }
    
    // Change the range in which an operator can set toll fees (in wei)
    // Example: operatorHub="0x8888", minTollBaseFees=1000, maxTollBaseFees=5000000
    function setIntervalTollFees(address operatorHub, uint minTollBaseFees, uint maxTollBaseFees)
        onlyOwner
        onlyIfOperatorHub(operatorHub)
        onlyIfRunning
        returns(bool success)
    {
        require(minTollBaseFees >= 0);
        require(maxTollBaseFees >= 0);
        require(minTollBaseFees < maxTollBaseFees);

        OperatorHub trustedOperatorHub = OperatorHub(operatorHub);
        return(trustedOperatorHub.setIntervalTollFees(minTollBaseFees, maxTollBaseFees));
    }    
    
    // Make a shareToRegulator proposal to an operator (between 0 and 100) 
    // Example: operatorHub="0x8888", shareToRegulator=50
    function setShareToRegulator(address operatorHub, uint shareToRegulator)
        onlyOwner
        onlyIfOperatorHub(operatorHub)
        onlyIfRunning
        returns(bool success)
    {
        require(shareToRegulator >= 0);
        require(shareToRegulator <= 100);

        OperatorHub trustedOperatorHub = OperatorHub(operatorHub);
        return(trustedOperatorHub.setShareToRegulator(shareToRegulator));
    }    

    // Change the owner of an operatorHub
    // Example: operatorHub="0x8888", newOperatorAddress="0x1111"
    function changeOperator(address operatorHub, address newOperatorAddress)
        onlyOwner
        onlyIfOperatorHub(operatorHub)
        onlyIfRunning
        returns(bool success)
    {
        require(newOperatorAddress != 0);

        OperatorHub trustedOperatorHub = OperatorHub(operatorHub);
        return(trustedOperatorHub.changeOwner(newOperatorAddress));
    }

    // Call from an operatorHub (msg.sender) to transfer a toll ownership to another operatorHub (newOperatorHubAddress)
    // Example: tollAddress="0x8888", newOperatorHubAddress="0x1111"
    function changeTollOwnership(address tollAddress, address newOperatorHubAddress)
        onlyIfOperatorHub(msg.sender)
        onlyIfOperatorHub(newOperatorHubAddress)
        onlyIfRunning
        returns(bool success)
    {
        OperatorHub trustedOperatorHub = OperatorHub(newOperatorHubAddress);
        return(trustedOperatorHub.addTollOwnership(tollAddress));
    }

    // Switch on/off an operator hub
    // Example: operatorHub="0x8888", onOff=0
    function runSwitchOperatorHub(address operatorHub, bool onOff)
        onlyOwner
        onlyIfOperatorHub(operatorHub)
        onlyIfRunning
        returns(bool success)
    {
        OperatorHub trustedOperatorHub = OperatorHub(operatorHub);
        return(trustedOperatorHub.runSwitch(onOff));
    }
    
    // 4.2. Control of the vehicleRegistry
    
    // Add a vehicle in the registry.
    // The vehicle owner will able to define a driver which would pay toll
    // Example: licencePlate="test8888", vehicleOwner="0x8888", vehicleType="Truck"
    function addVehicle(bytes32 licencePlate, address vehicleOwner, bytes32 vehicleType)
        onlyOwner
        onlyIfRunning
        returns(bool success)
    {
        require(licencePlate != "");
        require(vehicleType != "");
        require(vehicleOwner != 0);

        Vehicles trustedVehicleRegistry = Vehicles(vehicleRegistry);
        return(trustedVehicleRegistry.addVehicle(licencePlate, vehicleOwner, vehicleType));
    } 
    
    // Add or change a vehicle type and its extra fees charged everytime
    // Example: vehicleType="Truck", vehiculeTax=50
    function addChangeVehiculeType(bytes32 vehicleType, uint vehiculeTax)
        onlyOwner
        onlyIfRunning
        returns(bool success)
    {
        require(vehicleType != "");
        require(vehiculeTax > 0);

        Vehicles trustedVehicleRegistry = Vehicles(vehicleRegistry);
        return(trustedVehicleRegistry.addChangeVehiculeType(vehicleType, vehiculeTax));
    }

    // Switch on/off the vehicle registry
    // Example: onOff=0
    function runSwitchVehicleRegistry(bool onOff)
        onlyOwner
        onlyIfRunning
        returns(bool success)
    {
        Vehicles trustedVehicleRegistry = Vehicles(vehicleRegistry);
        return(trustedVehicleRegistry.runSwitch(onOff));
    }

}